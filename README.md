# Yahtzee

## Requirements

- [docker](https://docs.docker.com/get-docker/)
- [docker-compose](https://docs.docker.com/compose/install/)
- [make](https://www.gnu.org/software/make/)

## Docs

```bash
make
```

```bash
make doc
```

## Install

```bash
make install
```

## Tests

```bash
make tests-list
```

```bash
make tests
```

```bash
make test pattern="ClassTest::testName"
```

## Delete

```bash
make clean
```
