<?php

declare(strict_types=1);

namespace Ynov\Yahtzee\Tests\Data;

trait YahtzeeData
{
    /**
     * @return array [$dices, $scoreExpected]
     */
    public function dicesOnes(): array
    {
        return [
            [[2, 2, 2, 2, 2], 0],
            [[1, 2, 2, 2, 2], 1],
            [[1, 1, 2, 2, 2], 2],
            [[1, 1, 1, 2, 2], 3],
            [[1, 1, 1, 1, 2], 4],
            [[1, 1, 1, 1, 1], 5],
        ];
    }

    /**
     * @return array [$dices, $scoreExpected]
     */
    public function dicesTwos(): array
    {
        return [
            [[1, 1, 1, 1, 1], 0],
            [[2, 1, 1, 1, 1], 2],
            [[2, 2, 1, 1, 1], 4],
            [[2, 2, 2, 1, 1], 6],
            [[2, 2, 2, 2, 1], 8],
            [[2, 2, 2, 2, 2], 10],
        ];
    }

    /**
     * @return array [$dices, $scoreExpected]
     */
    public function dicesThrees(): array
    {
        return [
            [[1, 1, 1, 1, 1], 0],
            [[3, 1, 1, 1, 1], 3],
            [[3, 3, 1, 1, 1], 6],
            [[3, 3, 3, 1, 1], 9],
            [[3, 3, 3, 3, 1], 12],
            [[3, 3, 3, 3, 3], 15],
        ];
    }

    /**
     * @return array [$dices, $scoreExpected]
     */
    public function dicesFours(): array
    {
        return [
            [[1, 1, 1, 1, 1], 0],
            [[4, 1, 1, 1, 1], 4],
            [[4, 4, 1, 1, 1], 8],
            [[4, 4, 4, 1, 1], 12],
            [[4, 4, 4, 4, 1], 16],
            [[4, 4, 4, 4, 4], 20],
        ];
    }

    /**
     * @return array [$dices, $scoreExpected]
     */
    public function dicesFives(): array
    {
        return [
            [[1, 1, 1, 1, 1], 0],
            [[5, 1, 1, 1, 1], 5],
            [[5, 5, 1, 1, 1], 10],
            [[5, 5, 5, 1, 1], 15],
            [[5, 5, 5, 5, 1], 20],
            [[5, 5, 5, 5, 5], 25],
        ];
    }

    /**
     * @return array [$dices, $scoreExpected]
     */
    public function dicesSixes(): array
    {
        return [
            [[1, 1, 1, 1, 1], 0],
            [[6, 1, 1, 1, 1], 6],
            [[6, 6, 1, 1, 1], 12],
            [[6, 6, 6, 1, 1], 18],
            [[6, 6, 6, 6, 1], 24],
            [[6, 6, 6, 6, 6], 30],
        ];
    }

    /**
     * @return array [$dices, $scoreExpected]
     */
    public function dicesThreeOfAKind(): array
    {
        return [
            [[1, 1, 2, 2, 3], 0],
            [[1, 1, 1, 2, 2], 7],
            [[1, 1, 1, 1, 2], 6],
            [[1, 1, 1, 1, 1], 5],
        ];
    }

    /**
     * @return array [$dices, $scoreExpected]
     */
    public function dicesFourOfAKind(): array
    {
        return [
            [[1, 1, 1, 2, 2], 0],
            [[1, 1, 1, 1, 2], 6],
            [[2, 2, 2, 2, 1], 9],
        ];
    }

    /**
     * @return array [$dices, $scoreExpected]
     */
    public function dicesFullHouse(): array
    {
        return [
            [[1, 1, 1, 1, 2], 0],
            [[1, 1, 1, 2, 2], 25],
            [[1, 1, 1, 1, 1], 0],
            [[2, 2, 1, 1, 1], 25],
        ];
    }

    /**
     * @return array [$dices, $scoreExpected]
     */
    public function dicesSmallStraight(): array
    {
        return [
            [[1, 2, 3, 3, 3], 0],
            [[1, 2, 3, 4, 5], 30],
            [[2, 3, 4, 5, 2], 30],
            [[3, 4, 5, 6, 6], 30],
        ];
    }

    /**
     * @return array [$dices, $scoreExpected]
     */
    public function dicesLargeStraight(): array
    {
        return [
            [[1, 2, 3, 4, 6], 0],
            [[2, 3, 4, 5, 5], 0],
            [[1, 2, 3, 4, 5], 40],
            [[2, 3, 4, 5, 6], 40],
        ];
    }

    /**
     * @return array [$dices, $scoreExpected]
     */
    public function dicesChance(): array
    {
        return [
            [[1, 1, 1, 1, 1], 5],
            [[1, 2, 3, 4, 5], 15],
            [[2, 3, 4, 5, 6], 20],
            [[1, 3, 5, 2, 6], 17],
        ];
    }

    /**
     * @return array [$dices, $scoreExpected]
     */
    public function dicesYahtzee(): array
    {
        return [
            [[1, 2, 3, 4, 5], 0],
            [[1, 1, 1, 1, 1], 50],
            [[2, 2, 2, 2, 2], 50],
        ];
    }

    /**
     * @return array [$dices, $scoresExpected]
     */
    public function dicesCombinations(): array
    {
        return [
            [[1, 2, 3, 4, 5], [1, 2, 3, 4, 5, 0, 0, 0, 0, 30, 40, 15, 0]],
            [[1, 1, 1, 1, 1], [5, 0, 0, 0, 0, 0, 5, 5, 0, 0, 0, 5, 50]],
            [[1, 1, 2, 2, 2], [2, 6, 0, 0, 0, 0, 8, 0, 25, 0, 0, 8, 0]],
            [[1, 2, 2, 2, 2], [1, 8, 0, 0, 0, 0, 9, 9, 0, 0, 0, 9, 0]],
        ];
    }
}
