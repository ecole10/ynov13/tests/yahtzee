<?php

declare(strict_types=1);

namespace Ynov\Yahtzee\Tests;

use PHPUnit\Framework\TestCase;
use Ynov\Yahtzee\Tests\Data\YahtzeeData;
use Ynov\Yahtzee\Yahtzee;

class YahtzeeTest extends TestCase
{
    use YahtzeeData;

    /**
     * @dataProvider dicesOnes
     */
    public function testShouldReturnSumOfPointsForOnes(array $dices, int $scoreExpected): void
    {
        // Arrange
        $yahtzee = new Yahtzee();

        // Act
        $result = $yahtzee->sumOnes(dices: $dices);

        // Assert
        self::assertEquals(expected: $scoreExpected, actual: $result);
    }

    /**
     * @dataProvider dicesTwos
     */
    public function testShouldReturnSumOfPointsForTwos(array $dices, int $scoreExpected): void
    {
        // Arrange
        $yahtzee = new Yahtzee();

        // Act
        $result = $yahtzee->sumTwos(dices: $dices);

        // Assert
        self::assertEquals(expected: $scoreExpected, actual: $result);
    }

    /**
     * @dataProvider dicesThrees
     */
    public function testShouldReturnSumOfPointsForThrees(array $dices, int $scoreExpected): void
    {
        // Arrange
        $yahtzee = new Yahtzee();

        // Act
        $result = $yahtzee->sumThrees(dices: $dices);

        // Assert
        self::assertEquals(expected: $scoreExpected, actual: $result);
    }

    /**
     * @dataProvider dicesFours
     */
    public function testShouldReturnSumOfPointsForFours(array $dices, int $scoreExpected): void
    {
        // Arrange
        $yahtzee = new Yahtzee();

        // Act
        $result = $yahtzee->sumFours(dices: $dices);

        // Assert
        self::assertEquals(expected: $scoreExpected, actual: $result);
    }

    /**
     * @dataProvider dicesFives
     */
    public function testShouldReturnSumOfPointsForFives(array $dices, int $scoreExpected): void
    {
        // Arrange
        $yahtzee = new Yahtzee();

        // Act
        $result = $yahtzee->sumFives(dices: $dices);

        // Assert
        self::assertEquals(expected: $scoreExpected, actual: $result);
    }

    /**
     * @dataProvider dicesSixes
     */
    public function testShouldReturnSumOfPointsForSixes(array $dices, int $scoreExpected): void
    {
        // Arrange
        $yahtzee = new Yahtzee();

        // Act
        $result = $yahtzee->sumSixes(dices: $dices);

        // Assert
        self::assertEquals(expected: $scoreExpected, actual: $result);
    }

    /**
     * @dataProvider dicesThreeOfAKind
     */
    public function testShouldReturnSumOfPointsForThreeOfAKind(array $dices, int $scoreExpected): void
    {
        // Arrange
        $yahtzee = new Yahtzee();

        // Act
        $result = $yahtzee->sumThreeOfAKind(dices: $dices);

        // Assert
        self::assertEquals(expected: $scoreExpected, actual: $result);
    }

    /**
     * @dataProvider dicesFourOfAKind
     */
    public function testShouldReturnSumOfPointsForFourOfAKind(array $dices, int $scoreExpected): void
    {
        // Arrange
        $yahtzee = new Yahtzee();

        // Act
        $result = $yahtzee->sumFourOfAKind(dices: $dices);

        // Assert
        self::assertEquals(expected: $scoreExpected, actual: $result);
    }

    /**
     * @dataProvider dicesFullHouse
     */
    public function testShouldReturnSumOfPointsForFullHouse(array $dices, int $scoreExpected): void
    {
        // Arrange
        $yahtzee = new Yahtzee();

        // Act
        $result = $yahtzee->sumFullHouse(dices: $dices);

        // Assert
        self::assertEquals(expected: $scoreExpected, actual: $result);
    }

    /**
     * @dataProvider dicesSmallStraight
     */
    public function testShouldReturnSumOfPointsForSmallStraight(array $dices, int $scoreExpected): void
    {
        // Arrange
        $yahtzee = new Yahtzee();

        // Act
        $result = $yahtzee->sumSmallStraight(dices: $dices);

        // Assert
        self::assertEquals(expected: $scoreExpected, actual: $result);
    }

    /**
     * @dataProvider dicesLargeStraight
     */
    public function testShouldReturnSumOfPointsForLargeStraight(array $dices, int $scoreExpected): void
    {
        // Arrange
        $yahtzee = new Yahtzee();

        // Act
        $result = $yahtzee->sumLargeStraight(dices: $dices);

        // Assert
        self::assertEquals(expected: $scoreExpected, actual: $result);
    }

    /**
     * @dataProvider dicesChance
     */
    public function testShouldReturnSumOfPointsForChance(array $dices, int $scoreExpected): void
    {
        // Arrange
        $yahtzee = new Yahtzee();

        // Act
        $result = $yahtzee->sumChance(dices: $dices);

        // Assert
        self::assertEquals(expected: $scoreExpected, actual: $result);
    }

    /**
     * @dataProvider dicesYahtzee
     */
    public function testShouldReturnSumOfPointsForYahtzee(array $dices, int $scoreExpected): void
    {
        // Arrange
        $yahtzee = new Yahtzee();

        // Act
        $result = $yahtzee->sumYahtzee(dices: $dices);

        // Assert
        self::assertEquals(expected: $scoreExpected, actual: $result);
    }

    /**
     * @dataProvider dicesCombinations
     */
    public function testShouldReturnSumOfPointsForCombinations(array $dices, array $scoresExpected): void
    {
        // Arrange
        $yahtzee = new Yahtzee();

        // Act
        $result = $yahtzee->combinations(dices: $dices);

        // Assert
        foreach ($result as $key => $value) {
            self::assertEquals(expected: $scoresExpected[$key], actual: $value);
        }
    }

    /**
     * @doesNotPerformAssertions
     */
    public function testShouldPrintAllCombinations(): void
    {
        // Arrange
        $yahtzee = new Yahtzee();
        $combinations = [
            'Ones',
            'Twos',
            'Threes',
            'Fours',
            'Fives',
            'Sixes',
            'Three of a kind',
            'Four of a kind',
            'Full house',
            'Small straight',
            'Large straight',
            'Chance',
            'YAHTZEE',
        ];

        // Act
        $result = $yahtzee->play();

        // Assert
        foreach ($result as $key => $value) {
            echo sprintf("%s : %s \n", $combinations[$key], $value);
        }
    }
}
