<?php

declare(strict_types=1);

namespace Ynov\Yahtzee;

class Yahtzee
{
    private const DICE_VALUES = [1, 2, 3, 4, 5, 6];

    public function sumOnes(array $dices): int
    {
        return $this->sumNumber(dices: $dices, number: 1);
    }

    public function sumTwos(array $dices): int
    {
        return $this->sumNumber(dices: $dices, number: 2);
    }

    public function sumThrees(array $dices): int
    {
        return $this->sumNumber(dices: $dices, number: 3);
    }

    public function sumFours(array $dices): int
    {
        return $this->sumNumber(dices: $dices, number: 4);
    }

    public function sumFives(array $dices): int
    {
        return $this->sumNumber(dices: $dices, number: 5);
    }

    public function sumSixes(array $dices): int
    {
        return $this->sumNumber(dices: $dices, number: 6);
    }

    public function sumThreeOfAKind(array $dices): int
    {
        return $this->sumOfAKind(dices: $dices, minSameNumber: 3);
    }

    public function sumFourOfAKind(array $dices): int
    {
        return $this->sumOfAKind(dices: $dices, minSameNumber: 4);
    }

    public function sumFullHouse(array $dices): int
    {
        return $this->contains(needle: [2, 3], haystack: array_count_values($dices))
            ? 25
            : 0;
    }

    public function sumSmallStraight(array $dices): int
    {
        return $this->contains(needle: [1, 2, 3, 4], haystack: $dices)
        || $this->contains(needle: [2, 3, 4, 5], haystack: $dices)
        || $this->contains(needle: [3, 4, 5, 6], haystack: $dices)
            ? 30
            : 0;
    }

    public function sumLargeStraight(array $dices): int
    {
        return $this->contains(needle: [1, 2, 3, 4, 5], haystack: $dices)
        || $this->contains(needle: [2, 3, 4, 5, 6], haystack: $dices)
            ? 40
            : 0;
    }

    public function sumChance(array $dices): int
    {
        return $this->sum(dices: $dices);
    }

    public function sumYahtzee(array $dices): int
    {
        return count(array_count_values($dices)) === 1
            ? 50
            : 0;
    }

    public function combinations(array $dices): array
    {
        return [
            $this->sumOnes(dices: $dices),
            $this->sumTwos(dices: $dices),
            $this->sumThrees(dices: $dices),
            $this->sumFours(dices: $dices),
            $this->sumFives(dices: $dices),
            $this->sumSixes(dices: $dices),
            $this->sumThreeOfAKind(dices: $dices),
            $this->sumFourOfAKind(dices: $dices),
            $this->sumFullHouse(dices: $dices),
            $this->sumSmallStraight(dices: $dices),
            $this->sumLargeStraight(dices: $dices),
            $this->sumChance(dices: $dices),
            $this->sumYahtzee(dices: $dices),
        ];
    }

    public function play(): array
    {
        $randomDices = $this->randomDices();
        $this->printRandomDices(randomDices: $randomDices);
        return $this->combinations(dices: $randomDices);
    }

    private function sumNumber(array $dices, int $number): int
    {
        $dicesGroups = array_count_values($dices);

        return array_key_exists($number, $dicesGroups)
            ? $dicesGroups[$number] * $number
            : 0;
    }

    private function sumOfAKind(array $dices, int $minSameNumber): int
    {
        return array_filter(array_count_values($dices), fn($dicesGroup) => $dicesGroup >= $minSameNumber)
            ? $this->sum(dices: $dices)
            : 0;
    }

    private function sum(array $dices): int
    {
        return array_sum($dices);
    }

    private function contains(array $needle, array $haystack): bool
    {
        return count(array_diff($needle, $haystack)) === 0;
    }

    private function randomDices(): array
    {
        $randomDices = [];

        for ($i = 1; $i <= 5; $i++) {
            $randomDices[] = self::DICE_VALUES[array_rand(self::DICE_VALUES)];
        }

        return $randomDices;
    }

    private function printRandomDices(array $randomDices): void
    {
        echo "Dice => ";

        foreach ($randomDices as $randomDice) {
            echo $randomDice . ' ';
        }

        echo "\n";
    }
}
