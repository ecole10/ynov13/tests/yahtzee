DOCKER  		= docker
DOCKER_COMPOSE  = docker-compose
EXEC_PHP        = $(DOCKER_COMPOSE) run --rm php
COMPOSER        = $(EXEC_PHP) composer
PHPUNIT			= $(EXEC_PHP) vendor/bin/phpunit

##
##Project
##-------

install: ## Install dependencies
install: vendor

clean: ## Remove generated files
clean:
	-rm -rf ./vendor
	-docker rmi ynov-tests-yahtzee composer php:8.1.0-fpm-alpine

reset: ## Reinstall dependencies
reset: clean install

vendor:
	$(COMPOSER) install

##
##Tests
##-------

tests-list: ## List tests
tests-list: vendor
	$(PHPUNIT) tests --list-tests

tests: ## Run tests
tests: vendor
	$(PHPUNIT) tests
.PHONY: tests

test: ## Run specific test
test: vendor
	$(PHPUNIT) tests --filter $(pattern)

##
##Documentation
##-----

.DEFAULT_GOAL := doc
doc: ## List commands available
doc:
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
